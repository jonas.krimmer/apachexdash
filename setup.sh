#!/bin/bash
# Setup the containerized dash web application "Rechteckhohlleiter" to work with an Apache web server via http.

# install what is needed
DNF_CMD=$(which dnf)
if [[ ! -z $DNF_CMD ]]; then
    sudo dnf install podman podman-compose -y
else
    echo "error cannot install packages: please install podman, and podman-compose"
    exit 1;
fi

# make port 80 available
sudo sysctl net.ipv4.ip_unprivileged_port_start=0

# build and configure images
podman-compose up -d